const fs = require('fs')
const _ = require('lodash')

class Helpers {
  /**
   * Allows you to check if the file name is plural and put it otherwise
   * @param arg
   * @returns {string|*}
   */
  static pluralize(arg) {
    if (arg.includes('-')) return arg

    const end = arg.charAt(arg.length - 1).toLowerCase()

    switch (end) {
      case 's':
        return arg
      case 'y':
        return `${arg.slice(0, -1)}ies`
      default:
        return `${arg}s`
    }
  }

  /**
   * Check if there is an s at the end of the name
   * @param name
   * @returns {boolean}
   */
  static haveSToEnd(name) {
    return name.charAt(name.length - 1).toLowerCase() === 's'
  }

  static getArg() {
    return process.argv[2] || 'dv' // Default value `dv` if no args provided via CLI.
  }

  static getArgs() {
    process.argv.splice(0, 2)
    return process.argv
  }

  /**
   * Allows to change the name of the file to kebab-case
   * @param arg
   * @returns {string}
   */
  static fileNameFormater(arg) {
    return arg
      .replace(/([a-z])([A-Z])/g, '$1-$2')
      .replace(/[\s_]+/g, '-')
      .toLowerCase()
  }

  static toPascalCase(s) {
    return _.upperFirst(_.camelCase(s))
  }

  static writeFile(name, content) {
    fs.writeFile(name, content, (err) => {
      if (err) {
        throw new Error(err)
      }
    })
  }
}

module.exports = Helpers
