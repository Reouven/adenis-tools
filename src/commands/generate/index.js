const Helpers = require('../helpers')

const args = Helpers.getArgs()

/**
 * Selection of the file to generate
 */
switch (process.env.npm_lifecycle_event) {
  case 'generate:component':
    require('./component')(args[0])
    break
  case 'generate:page':
    require('./page')(args[0])
    break
  default:
    break
}
