const fs = require('fs')

const Helpers = require('../helpers')

/**
 * Allows you to generate a typical component
 * @param name
 */
module.exports = (name) => {
  if (!name) {
    throw new Error('Please enter the service name : --name toto')
  }
  if (fs.existsSync(`./components/${Helpers.toPascalCase(name)}.vue`)) {
    throw new Error('This component already exists')
  }

  const content = `<template>
  <div></div>
</template>

<script>
import { mapGetters } from 'vuex'
export default {
  name: 'Test',

  mixins: [],

  props: {},

  async asyncData({ params }) {
    // AsyncData est appelé à chaque fois avant le chargement du composant
  },

  data: () => ({}),

  async fetch() {
    /**
     * fetch is a hook called during server-side rendering after the component instance is created,
     * and on the client when navigating. The fetch hook should return a promise
     * (whether explicitly, or implicitly using  async / await) that will be resolved:
     */
  },

  head() {
    // Définit les balises Méta pour cette page
  },

  computed: {
    ...mapGetters(['loggedInUser']),
  },

  watch: {},

  async beforeCreate() {
    /*
    * Called synchronously in real time when the instance has been initialized, 
    * before knowledge observation and event/watcher setup.
    * /  
  },

  async created() {
    /**
     * Called synchronously once the instance is made. 
     * At this stage, the instance has finished process the choices which suggests the subsequent are set up:
     * information observation, computed properties, methods, watch/event callbacks. 
     * However, the mounting part has not been started, and therefore the $el property won’t be on the market nonetheless.
     */
  },

  async mounted() {
    /**
     * Called once the instance has been mounted, wherever component, passed to app.
     * mount is replaced by the fresh created vm.$el.
     * If the foundation instance is mounted to associate degree in-document component,
     * vm.$el also will be in-document once mounted is termed.
     */
  },

  methods: {},
}
</script>
<style scoped></style>
`

  Helpers.writeFile(`./components/${Helpers.toPascalCase(name)}.vue`, content)

  const success = [
    'background: green',
    'color: white',
    'display: block',
    'text-align: center',
  ].join(';')
  // eslint-disable-next-line no-console
  console.info('%c \n\n\nThe component have been created - SUCCESS !', success)
}
