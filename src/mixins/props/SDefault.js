export default {
  props: {
    dark: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    color: {
      required: false,
      type: String,
      default: () => '',
    },
    elevation: {
      required: false,
      type: [String, Number],
      default: () => 1,
    },
  },
}
