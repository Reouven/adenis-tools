export default {
  props: {
    headers: {
      required: true,
      type: Array,
    },
    items: {
      required: true,
      type: Array,
    },
  },
}
