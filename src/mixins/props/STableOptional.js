export default {
  props: {
    height: {
      type: Number,
      required: false,
      default: () => undefined,
    },
    elevation: {
      required: false,
      type: [Number, String],
      default: () => 1,
    },
    hideSearch: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    hideHeader: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    searchByColumn: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    fixedHeader: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    hideDefaultFooter: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    loading: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    edit: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    dense: {
      required: false,
      type: Boolean,
      default: () => true,
    },
    resizable: {
      required: false,
      type: Boolean,
      default: () => true,
    },
    showSelect: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    showExpand: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    singleSelect: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    singleExpand: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    disablePagination: {
      required: false,
      type: Boolean,
      default: () => false,
    },
    itemKey: {
      required: false,
      type: String,
      default: () => '',
    },
    searchValue: {
      required: false,
      type: String,
      default: () => '',
    },
    sortedBy: {
      required: false,
      type: Array,
      default: () => [],
    },
    sortedDesc: {
      required: false,
      type: Array,
      default: () => [],
    },
    actions: {
      required: false,
      type: Array,
      default: () => [],
    },
    actionsMultiples: {
      required: false,
      type: Array,
      default: () => [],
    },
    widthColumn: {
      required: false,
      type: String,
      default: () => '200px',
    },
    footerProps: {
      required: false,
      type: Object,
      default: () => ({
        'items-per-page-options': [10, 30, 50, 100, -1],
      }),
    },
    options: {
      required: false,
      type: Object,
      default: () => ({
        itemsPerPage: 10,
      }),
    },
    itemsPerPage: {
      required: false,
      type: [String, Number],
      default: () => 10,
    },
    colorRowHeaders: {
      required: false,
      type: Array,
      default: () => [],
    },
    value: {
      required: false,
      type: [String, Array, Boolean, Date, Object, Number],
      default: () => undefined,
    },
    pdfOptions: {
      required: false,
      type: Object,
      default: () => ({
        title: 'Report',
        format: 'a4',
        subheader: 'petit Report',
        orientation: 'l',
      }),
    },
  },
}
