export default {
  props: {
    label: {
      type: String,
      required: true,
      default: () => '',
    },
    text: {
      type: Boolean,
      default: () => false,
      required: false,
    },
    lowerCase: {
      type: Boolean,
      default: () => false,
      required: false,
    },
    outlined: {
      type: Boolean,
      default: () => false,
      required: false,
    },
    block: {
      type: Boolean,
      default: () => false,
      required: false,
    },
    disabled: {
      type: Boolean,
      default: () => false,
      required: false,
    },
    icon: {
      type: Boolean,
      default: () => false,
      required: false,
    },
    small: {
      type: Boolean,
      default: () => false,
      required: false,
    },
    xSmall: {
      type: Boolean,
      default: () => false,
      required: false,
    },
    large: {
      type: Boolean,
      default: () => false,
      required: false,
    },
    xLarge: {
      type: Boolean,
      default: () => false,
      required: false,
    },
  },
}
