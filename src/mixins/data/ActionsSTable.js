export default {
  data: () => ({
    actions: [
      {
        name: 'Editer',
        field: 'update',
        icon: 'pencil',
        color: 'sblue',
        function: '',
      },
      {
        name: 'Supprimer',
        field: 'delete',
        icon: 'delete',
        color: 'sred',
        function: '',
      },
    ],
  }),
}
