export default {
  methods: {
    /**
     * Permet d'appeler une fonction de manière dynamique
     * @param {*} functionName
     * @param  {...any} params
     */
    handler(functionName, ...params) {
      this[functionName](...params)
    },
  },
}
