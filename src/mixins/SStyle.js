export default {
  methods: {
    /**
     * Applique un style CSS à l'ensemble des DIV dont les selecteurs sont
     * passés en paramètre
     * @param selectors
     */
    applyStyle(...selectors) {
      selectors.forEach((selector) => this.$initColor(selector))
    },
  },
}
