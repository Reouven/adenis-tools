export default {
  methods: {
    async update(model, data) {
      const response = await this.$axios({
        method: 'put',
        url: `/api-name/${model}/update`,
        data,
      })

      if (response) {
        return response.data.values
      } else {
        return []
      }
    },
  },
}
