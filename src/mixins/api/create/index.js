export default {
  methods: {
    async create(model, data) {
      const response = await this.$axios({
        method: 'post',
        url: `/api-name/${model}/create`,
        data,
      })

      if (response) {
        return response.data.values
      } else {
        return 0
      }
    },
    // En back-end, il faut créer une route qui permet d'envoyer un mail
    async SendMail(model, data, user) {
      const response = await this.$axios({
        method: 'post',
        url: `/api-name/${model}/mail`,
        data: { ...data, user },
        // properties: {
        //   loading: false,
        //   toast: false,
        // },
      })
      if (response) {
        return response.data
      } else {
        return 0
      }
    },
  },
}
