export default {
  methods: {
    async removeItem(model, id) {
      const response = await this.$axios({
        method: 'delete',
        url: `/api-name/${model}/delete/${id}`,
      })

      if (response) {
        return response.data.values
      } else {
        return []
      }
    },
  },
}
