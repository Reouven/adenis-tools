export default {
  data: () => ({
    roles: [],
  }),
  async created() {
    await this.getRoles()
  },

  methods: {
    async getRoles() {
      const response = await this.$axios({
        method: 'get',
        url: `/api-name/roles/read/all`,
        // properties: {
        //   loading: false,
        //   toast: false,
        // },
      })

      if (response) {
        this.roles = response.data.values
      }
    },
  },
}
