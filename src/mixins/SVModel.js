export default {
  props: {
    value: {
      validator: (prop) =>
        typeof prop === 'string' ||
        typeof prop === 'number' ||
        typeof prop === 'boolean' ||
        typeof prop === 'object' ||
        Array.isArray(prop) ||
        prop === null ||
        prop === undefined,
      required: true,
      default: () => undefined,
    },
  },
  computed: {
    field: {
      get() {
        return this.value
      },
      set(field) {
        this.$emit('input', field)
      },
    },
  },
}
