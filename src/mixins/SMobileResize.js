export default {
  data: () => ({
    sMobileBreakpoint: false,
    sLargeBreakpoint: false,
    sHeightLargeBreakpoint: false,
  }),
  beforeDestroy() {
    if (typeof window === 'undefined') return

    window.removeEventListener('resize', this.onResize, { passive: true })
  },

  mounted() {
    this.onResize()

    window.addEventListener('resize', this.onResize, { passive: true })
  },
  methods: {
    /**
     * Verifie si l'ecran est un mobile ou non pour la responsivité
     */
    onResize() {
      this.sMobileBreakpoint = window.innerWidth < 600
      this.sLargeBreakpoint = window.innerWidth < 1600
      this.sHeightLargeBreakpoint = window.innerHeight < 750
    },
  },
}
