export default {
  login(context) {
    context.commit('login')
  },
  logout(context) {
    context.commit('logout')
  },
  updatePages(context, pages) {
    context.commit('update', { key: 'pages', value: pages })
  },
}
