/**
 * Prevent a user from browsing the site if he is not logged in and
 * to return to the login page if he is already logged in
 * @param store
 * @param redirect
 * @param route
 */

export default function ({ store, redirect, route }) {
  if (!process.server) {
    initTitle(route, store)
  }
  // guard
  // if (
  //   !userIsAdmin(store.state.auth.user, route) &&
  //   route.name &&
  //   route.name.startsWith('administration')
  // ) {
  //   redirect('/')
  //   store.dispatch('updatePages', [])
  // }

  if (store.state.auth.loggedIn) {
    if (mustBeRedirected(route, store)) {
      redirect(`/${store.state.part}`)
    }
  } else if (!route.name.includes('authentification')) {
    redirect('/authentification/login')
  }
}

/**
 * Check if the user should be redirected or not
 * @param store
 * @param route
 * @returns {boolean}
 */
function mustBeRedirected(route, store) {
  if (route.name === 'index' && store.state.part) {
    return true
  }
  return !!(
    !route.name ||
    route.name.includes('authentification/login') ||
    route.name.includes('authentification/register') ||
    route.name.includes('authentification/reset-password')
  )
}
/**
 * Allows you to initialize the title in the navbar
 * according to the page you are currently on
 * @param {*} route
 * @param {*} store
 */
function initTitle(route, store) {
  if (!route.fullPath.includes('authentification')) {
    const buffer = route.fullPath.split('/').filter((value) => !!value)
    const pages = buffer.map((page, i) => ({
      text: page,
      href: `/${buffer.slice(0, i + 1).join('/')}`,
    }))
    store.dispatch('updatePages', pages)
  } else {
    store.dispatch('updatePages', [])
  }
}

// const ADMIN_ROLE_ID = 1
// // const ADMIN_ROUTE_PREFIX = 'administration'

// function userIsAdmin(user, route) {
//   // Check if the user has the roles property
//   if (!user || !user.roles) {
//     return false
//   }
//   // Check if the roles property is an array and not empty
//   if (!Array.isArray(user.roles) || user.roles.length === 0) {
//     return false
//   }
//   // Check if the user has the admin role
//   if (user.roles[0].id !== ADMIN_ROLE_ID) {
//     return false
//   }
//   if (!route || !route.name) {
//     return false
//   }
//   return true
// }
