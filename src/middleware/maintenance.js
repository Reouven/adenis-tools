/**
 * Puts the site in maintenance mode and prevents any redirection
 * @param redirect
 * @param route
 * @returns {*}
 */
export default function ({ redirect, route }) {
  const isMaintenance = process.env.MAINTENANCE_MODE === 'true'
  if (isMaintenance) {
    return redirect('/maintenance')
  }
  if (isMaintenance === 'false' && route.path === '/maintenance') {
    return redirect('/')
  }
}
