import Vue from 'vue'

import SMobileResize from '../mixins/SMobileResize'
import SHandler from '../mixins/SHandler'

Vue.mixin(SMobileResize)
Vue.mixin(SHandler)
