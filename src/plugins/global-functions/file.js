import Vue from 'vue'
import XLSX from 'xlsx'
// import Toasted from 'vue-toasted'
const fs = require('fs')

// Vue.use(Toasted)

const isVerified = (app, file, type) => {
  const ext = file.name.split('.')
  if (ext.length > 2) {
    app.$stoast.error("ce type de fichier n'est pas autorisé")
    return false
  }
  return ext[ext.length - 1].toUpperCase().includes(type.toUpperCase())
}

export default ({ app }) => {
  /**
   * Allows you to write to a file whose name is passed as a parameter
   * @param file
   * @param value
   */
  Vue.prototype.$writeFile = (file, value) => {
    const data = JSON.stringify(value, null, 4)
    fs.writeFile(`/${file}`, data, (err) => {
      if (err) {
        throw err
      }
    })
  }

  /**
   * Allows you to recover data from a file whose name is passed as a parameter
   * @param file
   * @returns {array of object}
   */
  Vue.prototype.$readFile = (file) => {
    return JSON.parse(fs.readFileSync(`/${file}`))
  }

  /**
   * Allows you to retrieve data from an xlsx file whose name is passed as a
   * parameter and to return a JSON object
   * @param file
   * @returns {array of object}
   */
  Vue.prototype.$readXlsxToJson = (file) => {
    return new Promise((resolve, reject) => {
      if (!isVerified(app, file, 'xls')) {
        throw new Error('error')
      }
      const fileReader = new FileReader()
      fileReader.readAsArrayBuffer(file)

      fileReader.onload = (e) => {
        const bufferArray = e.target.result

        const wb = XLSX.read(bufferArray, { type: 'buffer', cellDates: true })

        const wsname = wb.SheetNames[0]

        const ws = wb.Sheets[wsname]

        const data = XLSX.utils.sheet_to_json(ws)

        return resolve(data)
      }

      fileReader.onerror = (error) => {
        reject(error)
      }
    })
  }
}
