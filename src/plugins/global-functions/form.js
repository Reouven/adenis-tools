import Vue from 'vue'

const callToast = (value) => {
  return !!value
}

export default ({ app }) => {
  /**
   * Permet de verifier si les formulaires ont bien été rempli
   * @param app
   * @returns {function(*=): this is *[]}
   */
  Vue.prototype.$formValidate = (data) => {
    if (!Array.isArray(data)) {
      data = [data]
    }

    return data.every((key) => {
      if (key.required && !callToast(key.value)) {
        app.$stoast.error(`Veuillez remplir le champ : "${key.props.label}"`)
        return false
      }
      return true
    })
  }
  /**
   * Permet de reinitialiser toutes les valeurs d'un formulaire à null
   * @param data
   */
  Vue.prototype.$resetForm = (data) => {
    if (!Array.isArray(data)) {
      data = [data]
    }
    data.forEach((form) => {
      form.value = null
    })
  }
  /**
   * Recupere les key-value d'un tableau de formulaire
   * @param forms
   * @returns {{}}
   */
  Vue.prototype.$getDataFromForm = (forms) => {
    if (!Array.isArray(forms)) {
      forms = [forms]
    }
    const data = {}
    forms.forEach((form) => {
      data[form.field] = form.value
    })

    return data
  }
  Vue.prototype.$initFormData = (values) => {
    const forms = []
    for (const [key, value] of Object.entries(values)) {
      if (!value.autoIncrement && key !== 'createdAt' && key !== 'updatedAt') {
        const type = value.type.toUpperCase()
        const name = type.includes('DATE')
          ? 'v-date-picker'
          : type.includes('TINYINT') || type.includes('BOOLEAN')
          ? 'v-switch'
          : 'v-text-field'
        forms.push({
          name,
          props: {
            label: key,
            ...(type.includes('INT') || type.includes('FLOAT')
              ? { type: 'number' }
              : {}),
          },
          field: key,
          value: value.defaultValue,
          required: !value.allowNull,
        })
      }
    }

    return JSON.parse(JSON.stringify(forms))
  }
}
