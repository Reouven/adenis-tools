import Vue from 'vue'
const moment = require('moment')
const FrenchWordsLib = require('french-words')
const FrenchWordsLefff = require('french-words-gender-lefff/dist/words.json')
/**
 * Ajoute une couleur sur une DIV donnée
 * @param el
 */
const applyColor = (el) => {
  if (el && el.style) {
    el.style.setProperty('color', process.env.DEFAULT_COLOR, 'important')
  }
}

export default ({ app }) => {
  /**
   * Permet de copier un tableau en profondeur par valeur et non pas par référence quelques soit le contenu de ce tableau
   * @param aObject
   * @returns {*[]|*}
   */
  Vue.prototype.$arrayCopy = (array) => {
    return JSON.parse(JSON.stringify(array))
  }

  /**
   * Permet de verifier si le paramètre fourni est une date
   * @param date
   * @returns {boolean}
   */
  Vue.prototype.$isDate = (date) => {
    return moment(
      date,
      [
        'DD/MM/YYYY',
        'DD/MM/YYYY hh:mm:ss',
        'DD/MM/YYYY hh:mm',
        'YYYY-MM-DD',
        moment.ISO_8601,
      ],
      true
    ).isValid()
  }
  /**
   * Permet d'ajouter n jour à une date
   * @param {*} date
   * @param {*} nbDay
   * @returns
   */
  Vue.prototype.$addDaysToDate = (date, nbDay) => {
    const res = new Date(date)
    res.setDate(date.getDate() + nbDay)

    return moment(res).format('YYYY-MM-DD')
  }

  /**
   * Permet de rendre unique un tableau d'objet selon la key passé en paramètre
   * @param arr
   * @param key
   * @returns {unknown[]}
   */
  Vue.prototype.$getUniqueListBy = (arr, key) => {
    return [...new Map(arr.map((item) => [item[key], item])).values()]
  }

  /**
   * Initialise la couleur par défaut au départ
   * @param id
   */
  Vue.prototype.$initColor = (id) => {
    const elementById = document.getElementById(id)
    const elementByClass = document.getElementsByClassName(id)

    applyColor(elementById)
    applyColor(elementByClass)
  }

  /**
   * Permet de transfomer une chaine de caractère au format
   * kebab-case
   * @param {*} str
   * @returns
   */
  Vue.prototype.$toKebabCase = (str) => {
    if (!str) {
      return ''
    }
    const newStr = str
      .match(
        /[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g
      )
      .map((x) => x.toLowerCase())
      .join('-')

    return newStr
  }

  /**
   * Permet de récuperer le bon parent d'un component
   * @param {*} parent
   * @param {*} data
   * @returns
   */
  Vue.prototype.$getParent = (parent, data) => {
    let p = parent
    while (!p[data]) {
      p = p.$parent
    }
    return p
  }
  /**
   * Permet de retourner une chaine en la tronquant
   * @param {*} parent
   * @param {*} data
   * @returns
   */
  Vue.prototype.$truncate = (input, max = 15) => {
    if (!input) return input
    return input.toString().length > max
      ? `${input.toString().substring(0, max)}...`
      : input
  }

  /**
   * Permet d'initialiser le nom des collones d'un tableau en
   * fonction de ces champs
   * @param {*} items
   * @param {*} widths
   * @returns
   */
  Vue.prototype.$initHeaders = (items, widths = []) => {
    const data = []

    if (!items.length) {
      return data
    }
    // const keys = ['id', 'createdAt', 'updatedAt']
    Object.entries(items[0]).forEach(([key, value], index) => {
      if (
        // !keys.includes(key) &&
        (!key.includes('Id') && typeof value !== 'object') ||
        value === null
      ) {
        data.push({
          text: key,
          value: key,
          width: widths[index] ? widths[index] + '%' : '',
        })
      }
    })
    return data
  }

  /**
   * Calcul la somme d'un champ d'un tableau d'objet passé en paramètre
   * @param {*} array
   * @param {*} field
   * @returns
   */
  Vue.prototype.$sumOfFieldOfArray = (array, field) => {
    return array
      .map((item) => item[field])
      .reduce((prev, curr) => prev + curr, 0)
  }
  /**
   * Convertir des octets
   * @param {*} aSize
   * @returns
   */
  Vue.prototype.$fileConvertiSize = (aSize, toFixed = 2) => {
    aSize = Math.abs(parseInt(aSize, 10))
    const def = [
      [1, 'octets'],
      [1024, 'ko'],
      [1024 * 1024, 'Mo'],
      [1024 * 1024 * 1024, 'Go'],
      [1024 * 1024 * 1024 * 1024, 'To'],
    ]
    for (let i = 0; i < def.length; i++) {
      if (aSize < def[i][0])
        return (aSize / def[i - 1][0]).toFixed(toFixed) + ' ' + def[i - 1][1]
    }
  }
  /**
   * Permet de savoir si un nombre est négatif
   * @param {*} field
   * @returns
   */
  Vue.prototype.$isNegative = (field) => {
    return field.charAt(0) === '-'
  }

  /**
   * Calcul la somme d'un champ d'un tableau d'entier
   * @param {*} array
   */
  Vue.prototype.$sumOfArray = (array) =>
    array.reduce((previousValue, currentValue) => previousValue + currentValue)

  /**
   * permet de connaitre le genre d'un mot
   * @param {*} str
   * @returns
   */
  Vue.prototype.$getGender = (str) => {
    try {
      return FrenchWordsLib.getGender(null, FrenchWordsLefff, str) === 'F'
    } catch (e) {
      return -1
    }
  }

  /**
   * Permet de transformer la première lettre d'un mot en majuscule
   * @param {*} s
   * @returns
   */
  Vue.prototype.$firstLetterUppercase = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }
  /**
   * Allows you to put a character string in the plural
   * @param {*} str
   * @returns
   */
  Vue.prototype.$pluralize = (str) => {
    if (str.includes('-')) return str
    const end = str.charAt(str.length - 1).toLowerCase()

    switch (end) {
      case 's':
        return str
      case 'y':
        return `${str.slice(0, -1)}ies`
      default:
        return `${str}s`
    }
  }
  /**
   * Allow to merge two array of object by a key
   * @param {*} byKey
   * @param  {...any} lists
   * @returns
   */
  Vue.prototype.$mergeArrayObjects = (byKey, ...lists) =>
    Object.values(
      lists.reduce((idx, list) => {
        list.forEach((record) => {
          // eslint-disable-next-line no-param-reassign
          if (idx[record[byKey]])
            idx[record[byKey]] = Object.assign(idx[record[byKey]], record)
          // eslint-disable-next-line no-param-reassign
          else idx[record[byKey]] = record
        })
        return idx
      }, {})
    )
}
