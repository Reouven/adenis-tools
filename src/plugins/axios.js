export default ({ store, $axios, $auth, app, error: nuxtError }) => {
  $axios.onRequest((config) => {
    if (!config.properties || config.properties.loading) {
      app.$bus.$emit('is-loading', true)
    }
  })
  $axios.onResponse((response) => {
    app.$bus.$emit('is-loading', false)
    if (
      !response.config.url.includes('auth/me') &&
      (!response.config.properties || response.config.properties.toast)
    ) {
      app.$stoast.success(response.data.message)
    }
  })
  $axios.onError((error) => {
    const {
      response: {
        data: { message },
      },
    } = error
    if (error.response.status === 401) {
      app.$auth.logout()
      app.router.push('/authentification/login')
    }
    app.$bus.$emit('is-loading', false)
    if (
      !error.response.config.properties ||
      error.response.config.properties.toast === undefined
    ) {
      app.$stoast.error(message)
    }
    return Promise.resolve(false)
  })
}
