const toast = (app) => ({
  success(message) {
    app.$bus.$emit('toast', 'success', message)
  },
  error(message) {
    app.$bus.$emit('toast', 'error', message)
  },
  info(message) {
    app.$bus.$emit('toast', 'info', message)
  },
  warning(message) {
    app.$bus.$emit('toast', 'warning', message)
  },
})

export default ({ app }, inject) => {
  inject('stoast', toast(app))
}
