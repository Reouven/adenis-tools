import colors from 'vuetify/es5/util/colors'
import fr from 'vuetify/es5/locale/fr'
import pkg from './package'

require('dotenv-flow').config({
  node_env: process.env.NODE_ENV,
  path: '.environments',
})

export default {
  target: 'static',
  env: {
    DEFAULT_COLOR: process.env.DEFAULT_COLOR || '#36495D',
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s',
    title: pkg.name,
    htmlAttrs: {
      lang: 'fr',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  loading: false,

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/css/main.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/lodash',
    '~/plugins/click-outside',
    '~/plugins/event-bus',
    '~/plugins/s-toast',
    '~/plugins/global-functions/utils',
    '~/plugins/global-mixins',
    { src: '~/plugins/axios', mode: 'client' },
    { src: '~/plugins/global-functions/form', mode: 'client' },
    { src: '~/plugins/global-functions/file', mode: 'client' },
    { src: '~/plugins/vuex-persist', mode: 'client' },
    { src: '~/plugins/vue-apexchart', mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: {
    dirs: ['~/components/utils'],
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    // https://github.com/nuxt-community/moment-module#readme
    '@nuxtjs/moment',
    '@nuxtjs/axios',
  ],

  moment: {
    locales: ['fr'],
    timezone: true,
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/auth-next', 'vue2-editor/nuxt'],

  axios: {
    proxy: true,
  },

  proxy: {
    '/api-gouv/': {
      target: process.env.GOUV_URL,
      pathRewrite: { '^/api-gouv/': '' },
      changeOrigin: true,
    },
    '/api-name/': {
      target: process.env.API_NAME,
      pathRewrite: { '^/api-name/': '' },
      changeOrigin: true,
    },
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    lang: {
      locales: { fr },
      current: 'fr',
    },
    theme: {
      dark: false,
      themes: {
        dark: {
          default: process.env.DEFAULT_COLOR,
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
          syellow: '#E69F0B',
          sblue: '#2B9B86',
          sgreen: '#78e58e',
          sred: '#B41026',
        },
        light: {
          primary: colors.blue.lighten2,
          accent: colors.grey.lighten3,
          secondary: colors.amber.lighten3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
          default: process.env.DEFAULT_COLOR,
          syellow: '#E69F0B',
          sgreen: '#78e58e',
          sblue: '#2B9B86',
          sred: '#B41026',
        },
      },
    },
  },

  router: {
    middleware: [/* 'redirect', */ 'maintenance'],
  },

  generate: {
    dir: 'dist',
  },
  // auth: {
  //   strategies: {
  //     local: {
  //       token: {
  //         property: 'token', // the token property should be set here, `propertyName` in endpoint was deprecated.
  //         type: 'Bearer', // should be "Bearer" if is jwt token
  //       },
  //       endpoints: {
  //         login: {
  //           url: '/api-name/auth/login',
  //           method: 'post',
  //           propertyName: 'data.token',
  //         },
  //         user: {
  //           url: '/api-name/auth/me',
  //           method: 'get',
  //           propertyName: false,
  //         },
  //         logout: false,
  //       },
  //       user: {
  //         property: false,
  //       },
  //     },
  //   },
  //   redirect: {
  //     login: '/authentification/login',
  //     logout: '/authentification/login',
  //     home: '/',
  //   },
  //   tokenRequired: true,
  //   tokenType: 'Bearer',
  //   watchLoggedIn: true,
  //   rewriteRedirects: true,
  // },
  vue: {
    config: {
      productionTip: true,
      devtools: false,
    },
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['vuetify/lib', 'global-mixins'],
    extend(config, ctx) {
      config.node = {
        fs: 'empty',
      }
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true,
          },
        })
      }
    },
  },
}
